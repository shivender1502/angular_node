import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ParentComponent } from 'src/app/Components/parent/parent.component';
import { AppComponent } from 'src/app/app.component';
import { HomeComponent } from 'src/app/Components/home/home.component';

const routes: Routes = [
  { path: 'ComponentRelation',      component: ParentComponent },
  { path: 'home',      component: HomeComponent },
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
